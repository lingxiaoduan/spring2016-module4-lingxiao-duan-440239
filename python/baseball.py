import re
## Our regex rule
regex = re.compile("([A-Z]\w+ [A-Z]\w+)+ batted+ (\d+) times with+ (\d+) (?:.+)")

while True:
    ##getting the file name
    year= raw_input("Which year's data do you want to check? (format: ####)" )
    filename = "data/cardinals-"+year+".txt"  
    ##print filename

    ##store data in a dictionary
    stats = {}

    ##open and read the data file
    ##dictioinary format: {name: [#batted, #hits]
    try:
        f = open(filename,'r')
        
        while True:
            l = f.readline()
        ##    print l
            if l != '':
                match = regex.match(l)
                if match != None:
                    if match.group(1) in stats.keys():
                        stats[match.group(1)][0] = stats[match.group(1)][0]+int(match.group(2))
                        stats[match.group(1)][1] = stats[match.group(1)][1]+int(match.group(3))
                    else:
                        stats[match.group(1)] = [int(match.group(2)),int(match.group(3))]
            else:
                break
            
        score = {}
        for i in range(len(stats)):
            score[stats.keys()[i]] = round(float(stats[stats.keys()[i]][1])/stats[stats.keys()[i]][0],3)
        score = sorted(score.items(),key=lambda x: (-x[1],x[0]))

        for i in range(len(score)):
            print score[i][0],":",score[i][1]



    except:
        print "Failed to open file"

    q = raw_input("Want to see another year's data?(Y/N)")
    if q in "Nn":
        break
    



