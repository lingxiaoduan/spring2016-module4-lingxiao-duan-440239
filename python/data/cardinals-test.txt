# The information used here was obtained free of
# charge from and is copyrighted by Retrosheet.  Interested
# parties may contact Retrosheet at "www.retrosheet.org".

=== Chicago Cubs vs. St.Louis Cardinals, 1930-04-15 ===
Taylor Douthit batted 3 times with 1 hits and 0 runs
Sparky Adams batted 4 times with 0 hits and 0 runs
Frankie Frisch batted 4 times with 2 hits and 1 runs
Jim Bottomley batted 4 times with 1 hits and 0 runs
Chick Hafey batted 4 times with 1 hits and 1 runs
Showboat Fisher batted 5 times with 4 hits and 2 runs
Charlie Gelbert batted 4 times with 2 hits and 2 runs
Jimmie Wilson batted 3 times with 1 hits and 2 runs
Flint Rhem batted 1 times with 0 hits and 0 runs

=== Chicago Cubs vs. St.Louis Cardinals, 1930-04-16 ===
Taylor Douthit batted 6 times with 2 hits and 1 runs
Sparky Adams batted 4 times with 1 hits and 1 runs
Frankie Frisch batted 4 times with 3 hits and 4 runs
Jim Bottomley batted 4 times with 3 hits and 3 runs
Chick Hafey batted 4 times with 2 hits and 2 runs
Showboat Fisher batted 4 times with 4 hits and 1 runs
Charlie Gelbert batted 3 times with 2 hits and 1 runs
Jimmie Wilson batted 5 times with 2 hits and 0 runs
Syl Johnson batted 5 times with 1 hits and 0 runs

=== Chicago Cubs vs. St.Louis Cardinals, 1930-04-17 ===
Taylor Douthit batted 3 times with 1 hits and 0 runs
Sparky Adams batted 3 times with 1 hits and 0 runs
Frankie Frisch batted 3 times with 1 hits and 0 runs
Jim Bottomley batted 2 times with 0 hits and 0 runs
Chick Hafey batted 2 times with 0 hits and 0 runs
Showboat Fisher batted 2 times with 0 hits and 0 runs
Charlie Gelbert batted 2 times with 0 hits and 0 runs
Earl Smith batted 1 times with 0 hits and 0 runs
Hi Bell batted 2 times with 0 hits and 0 runs